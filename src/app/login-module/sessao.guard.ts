import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionService } from './services/session.service';

@Injectable()
export class SessaoGuard implements CanActivate {
  
  constructor(private sessao : SessionService, private router : Router) {}
  
  canActivate() {
    if(this.sessao.getLoggedUser()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  } 
}

