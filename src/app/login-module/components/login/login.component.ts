import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email;
  senha;
  message : string = null;
  invalidInput : boolean = false;

  constructor(
    private session : SessionService,
    private router : Router
  ) { }

  ngOnInit() {
    if(this.session.getLoggedUser()) {
      this.router.navigate(['contato']);
    }
  }

  onSubmitForm() {
    if(this.isInputValid()) {
      this.session.logIn(this.email, this.senha).subscribe((logged) => {
        if(logged) {
          this.router.navigate(['contato']);
        } else {
          this.message = "Senha e/ou email inválidos.";
        }
      });
    } else {
      this.message = "Senha e/ou email inválidos.";
      setTimeout(() => {
        this.message = null;
      }, 2000);
    }
  }

  isInputValid() : boolean {
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(this.email) && this.senha.length;
  }

}
