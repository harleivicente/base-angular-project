import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared-module/shared.module';

import { LoginComponent } from './components/login/login.component';

import { SessaoGuard } from './sessao.guard';
import { SessionService } from './services/session.service';

const routes: Routes = [
  {path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginComponent],
  providers: [
    SessaoGuard,
    SessionService
  ]
})
export class LoginModule { }
