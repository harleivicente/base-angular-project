import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LocalStorageService } from '../../shared-module/services/local-storage.service';
import { Usuario } from '../../models/Usuario';

@Injectable()
export class SessionService {
  ROOT_EMAIL : string = "root@email.com";
  ROOT_PASSWORD : string = "root";

  constructor(
    private localStore : LocalStorageService
  ) {}

  /**
   * @return <null | Usuario>
   */
  getLoggedUser() : Usuario {
      let user = this.localStore.getValue('session:user');
      return user ? user : null;
  }

  /**
   * 
   */
  logOut() {
    this.localStore.setValue('session:user', null);
  }

  logIn(email, password) : Observable<boolean> {
    return Observable.create(observer => {
      if(email == this.ROOT_EMAIL && password == this.ROOT_PASSWORD) {
        let loggedUser = new Usuario({email: email, senha: password});
        this.localStore.setValue('session:user', loggedUser);
        observer.next(true);
      } else {
        observer.next(false);  
      }
      observer.complete();
    });
  }


}
