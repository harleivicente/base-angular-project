

export class Contato {
    id: number = 0;
    nome: string = "";
    telefone: string = "";
    email: string = "";
    endereco: string = "";
    foto: string = "";
    data_nascimento: string = "";
    data_cadastro: string = "";

    constructor(dados) {
        this.id = dados.id;
        this.nome = dados.nome;
        this.telefone = dados.telefone;
        this.email = dados.email;
        this.endereco = dados.endereco;
        this.foto = dados.foto;
        this.data_nascimento = dados.data_nascimento;
        this.data_cadastro = dados.data_cadastro;
        
        for (var key in this) {
            if (this.hasOwnProperty(key) && this[key] == undefined) {
                this[key] = "";
            }
        }
    }

}