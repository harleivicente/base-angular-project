import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared-module/shared.module';

import { ContactListComponent } from './components/contact-list/contact-list.component';
import { ContactEditComponent } from './components/contact-edit/contact-edit.component';
import { ContactCreateComponent } from './components/contact-create/contact-create.component';
import { ContactViewComponent } from './components/contact-view/contact-view.component';
import { ContactRootComponent } from './components/contact-root/contact-root.component';
import { OrderArrowComponent } from './components/order-arrow/order-arrow.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

// Guard
import { SessaoGuard } from '../login-module/sessao.guard';

const routes: Routes = [
  {path: 'contato', component: ContactRootComponent, 
  canActivate: [ SessaoGuard ],
  children: [
    {path: '', component: ContactListComponent},
    {path: 'criar', component: ContactCreateComponent},
    {path: ':id/editar', component: ContactEditComponent},
    {path: ':id', component: ContactViewComponent}
  ]}
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ContactListComponent,
    ContactEditComponent,
    ContactCreateComponent,
    ContactViewComponent,
    ContactRootComponent,
    OrderArrowComponent,
    ContactFormComponent
  ],
  providers: []
})
export class ContactModule { }
