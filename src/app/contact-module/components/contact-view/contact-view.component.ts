import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContactService } from '../../../shared-module/services/contact.service';
import { Contato } from '../../../models/Contato';

@Component({
  selector: 'app-contact-view',
  templateUrl: './contact-view.component.html',
  styleUrls: ['./contact-view.component.scss']
})
export class ContactViewComponent implements OnInit {
  private openModal : EventEmitter<string> = new EventEmitter();
  private contactToRemoveId;

  constructor(
    private router : Router,
    private route : ActivatedRoute,
    private contactService : ContactService
  ) { }

  private contact : Contato;
  
  removeContact(contato) {
    this.contactToRemoveId = contato.id;
    this.openModal.emit(`Tem certeza que deseja remover o contato: ${contato.nome} ?`);
  }

  modalClosed(result) {
    if(result == "confirm"){
      this.contactService.removeById(this.contactToRemoveId).subscribe(() => {
          this.router.navigate(['contato']);
      });
    }
  }
  ngOnInit() {

    this.route.params.subscribe(params => {
      params = params as any;

      this.contactService.getById(params.id).subscribe(contact => {
        if(contact) {
          this.contact = contact;
        } else {
          this.router.navigate(['contato']);
        }
      });

    });

  }

}
