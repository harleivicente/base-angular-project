import { Component, OnInit, EventEmitter } from '@angular/core';
import { Contato } from '../../../models/Contato';
import { Router } from '@angular/router';
import { ContactService } from '../../../shared-module/services/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  fields: {name : string, text: string, date?: boolean, mobile?: boolean}[] = [
    {name: 'id', text: '#ID', mobile: false},
    {name: 'nome', text: 'Nome', mobile: true},
    {name: 'email', text: 'Email', mobile: false},
    {name: 'data_nascimento', text: 'Data nascimento', date: true, mobile: false}
  ];
  private perPage : number = 3;
  private orderByField : string;
  private currentPage : number = 1;
  private orderByAsc : boolean;
  private data : Contato[] = [];
  private orderedPagedData : Contato[] = [];
  private openModal : EventEmitter<string> = new EventEmitter();

  private contactToRemoveId;

  constructor(
    private contactService : ContactService,
    private router : Router
  ) { }

  modalClosed(result) {
    if(result == "confirm"){
      this.contactService.removeById(this.contactToRemoveId).subscribe(() => {
          this.fetchOrderPageData();
      });
    }
  }
  
  /**
  * 
  * @param name field name
  */
  setOrderByField(name : string) {
    if(this.orderByField != name) {
      this.orderByField = name;
      this.orderByAsc = true;
    } else {
      this.orderByAsc = !this.orderByAsc;
    }

    this.updateOrderAndPaginate();
  }

  /**
   * Updates this.orderedPagedData
   */
  updateOrderAndPaginate () {

    // Sort
    this.data.sort((a, b) => {
      let compare_result;
      if(a[this.orderByField] > b[this.orderByField]) {
        compare_result = 1;
      } else {
        compare_result = -1;
      }
      if(!this.orderByAsc) {
        compare_result = compare_result == 1 ? -1 : 1;
      }
      return compare_result;
    });
    
    let start : number = (this.currentPage-1) * this.perPage;
    this.orderedPagedData = this.data.slice(start, start + this.perPage);
  }

  removeContact(contato) {
    this.contactToRemoveId = contato.id;
    this.openModal.emit(`Tem certeza que deseja remover o contato: ${contato.nome} ?`);
  }
  
  /**
   * Fetch data from local storage, orders and pages 
   * according to UI.
   * 
   */
  fetchOrderPageData() {
    this.contactService.getAll().subscribe(data => {
      this.data = data;
      this.updateOrderAndPaginate();
    });
  }

  ngOnInit() {
    this.fetchOrderPageData();
    this.setOrderByField('nome');
  }
  
}
