import { Component, OnInit, EventEmitter } from '@angular/core';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { ContactService } from '../../../shared-module/services/contact.service';
import { Contato } from '../../../models/Contato';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {
  form : EventEmitter<any> = new EventEmitter();
  contato : Contato;

  constructor(
    private route:ActivatedRoute,
    private router : Router,
    private contactService : ContactService
  ) { }


  onFormReady() {
    this.form.emit(this.contato);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      params = params as any;
      let contato : Contato;
      
      if(params.id) {
        this.contactService.getById(params.id).subscribe(contato => {
          if(contato) {
            this.contato = contato;
          } else {
            this.router.navigate(['contato']);
          }
        });
      } else {
        this.router.navigate(['contato']);
      }

    });
  }

  formSubmit(contatoEditado) {
    let dataNasc = contatoEditado.data_nascimento;
    let dataNascDate : Date = new Date(dataNasc.year, dataNasc.month - 1, dataNasc.day);
    contatoEditado.data_nascimento = dataNascDate.getTime();
    let newContato = new Contato(contatoEditado);    
    this.contactService.updateById(contatoEditado.id, newContato).subscribe(ok => {
      if(ok) {
        this.router.navigate(['contato']);
      }
    });
  }

}
