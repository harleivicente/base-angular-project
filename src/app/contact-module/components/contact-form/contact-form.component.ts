import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, ValidatorFn, AbstractControl } from '@angular/forms';
import { Contato } from '../../../models/Contato';
import { Observable } from 'rxjs/Observable';
import { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  @Input() title : string;
  @Input() dados : EventEmitter<Contato> = new EventEmitter();
  @Output() formOut : EventEmitter<any> = new EventEmitter();
  @Output() formReady : EventEmitter<boolean> = new EventEmitter();
  
  private form : FormGroup;
  private dataNascimento; // {year: int, month: int, day : int}
  private formImageUrl;
  private formSubmitAttempt : boolean = false;
  private dateIsValid : boolean = true;
  
  constructor(private formBuilder : FormBuilder) {}
  
  ngOnInit() {
    this.buildForm();
    
    this.dados.subscribe(contatoEdit => {
      if(!contatoEdit) {
        this.form.reset();
      } else {
        // Fill out values
        this.form.setValue(contatoEdit);

        // Fill out data nascimento
        this.dataNascimento = contatoEdit.data_nascimento;
        let dataNascDate : Date = new Date(contatoEdit.data_nascimento);
        this.dataNascimento = {
          year: dataNascDate.getFullYear(),
          month: dataNascDate.getMonth() + 1,
          day: dataNascDate.getDate()
        };

        // Fill out image
        this.formImageUrl = contatoEdit.foto;

      }
    });
    
    this.formReady.emit(true);
  }
  
  onSubmitForm () {
    this.formSubmitAttempt = true;
    this.validateData();
    if(this.form.valid && this.dateIsValid) {
      let formValues = this.form.value;
      formValues.data_nascimento = this.dataNascimento;
      this.formOut.emit(formValues);
    }
  }
  
  imageChange(event) {
    let input = event.target;
    let file = input.files.length ? input.files[0] : false;
    
    if(FileReader && file) {
      
      // Load image into form as string
      let fileReader = new FileReader();
      fileReader.onload = () => {
        this.formImageUrl = fileReader.result;
        this.form.get('foto').setValue(fileReader.result);
      }
      fileReader.readAsDataURL(file);
    }      
  }
  
  buildForm() {
    this.form = this.formBuilder.group({
      id : '',
      foto : '',
      data_cadastro : '',
      data_nascimento : '',
      nome: ['', Validators.required],
      telefone: '',
      email: ['', [Validators.required, validateEmail()]],
      endereco: ''
    });     
  }

  validateData() {
    let valid = this.dataNascimento && this.dataNascimento.year > 0 && this.dataNascimento.month > 0 && this.dataNascimento.day > 0;
    this.dateIsValid = valid;
  }

  get nome() { return this.form.get('nome'); }
  get email() { return this.form.get('email'); }

}

function validateEmail(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return !regex.test(control.value) ? {'invalidEmail' : {value: control.value}} : null;
  };
}

