import { Component, OnInit, EventEmitter } from '@angular/core';
import { Contato } from '../../../models/Contato';
import { Router } from '@angular/router';
import { ContactService } from '../../../shared-module/services/contact.service';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {
  private formInput : EventEmitter<Contato> = new EventEmitter();
  
  constructor(
    private contatoService : ContactService,
    private router : Router
  ) {}

  ngOnInit() {
    this.formInput.emit(null);
  }

  formSubmitData(data) {
    let dobo = data.data_nascimento;
    let dob : Date = new Date(dobo.year, dobo.month - 1, dobo.day);
    let now : Date = new Date();
    data.data_nascimento = dob.getTime();
    data.data_cadastro = now.getTime();
    let newContato = new Contato(data);
    
    this.contatoService.add(newContato).subscribe(ok => {
      if(ok) {
        this.router.navigate(['contato']);
      }
    });
  }

}
