import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderArrowComponent } from './order-arrow.component';

describe('OrderArrowComponent', () => {
  let component: OrderArrowComponent;
  let fixture: ComponentFixture<OrderArrowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderArrowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderArrowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
