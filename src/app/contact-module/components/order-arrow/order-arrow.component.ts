import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-arrow',
  templateUrl: './order-arrow.component.html',
  styleUrls: ['./order-arrow.component.scss']
})
export class OrderArrowComponent implements OnInit {
  @Input() show : boolean = false;
  @Input() ascending : boolean = false;

  constructor() { }

  ngOnInit() {

  }

}
