import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// Modules
import { SharedModule } from './shared-module/shared.module';
import { ContactModule } from './contact-module/contact.module';
import { LoginModule } from './login-module/login.module';

import { AppComponent } from './app-component/app.component';

const routes: Routes = [
    {path: '', redirectTo: 'contato', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    NgbModule.forRoot(),
    LoginModule,
    BrowserModule,
    SharedModule,
    ContactModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
