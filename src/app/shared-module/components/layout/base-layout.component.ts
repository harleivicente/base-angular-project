import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../../../login-module/services/session.service';
import { Usuario } from '../../../models/Usuario';

@Component({
    selector: 'base-layout',
    templateUrl: 'base-layout.component.html',
    styleUrls: ['base-layout.component.scss']
})

export class BaseLayoutComponent implements OnInit {
    user : Usuario;

    constructor(
        private session : SessionService,
        private router : Router
    ) { }

    logout() {
        this.session.logOut();
        this.router.navigate(['login']);
    }

    ngOnInit() { 
        this.user = this.session.getLoggedUser();
    }
}