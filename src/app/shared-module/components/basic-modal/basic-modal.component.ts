import {Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-basic-modal',
  templateUrl: './basic-modal.component.html',
  styleUrls: ['./basic-modal.component.scss']
})
export class BasicModalComponent implements OnInit {
  @Input() open : EventEmitter<string> = new EventEmitter();
  @Output() result : EventEmitter<string> = new EventEmitter();
  @ViewChild("modalRef") modalRef;
  private content : string = "";

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.open.subscribe(content => {
      this.content = content;
      this.modalService.open(this.modalRef).result.then(
        result => {
          this.result.emit(result);
        },
        reason => {
          this.result.emit(reason);
        }
      );
    });
  }

}
