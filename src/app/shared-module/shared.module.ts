import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ContactService } from './services/contact.service';
import { LocalStorageService } from './services/local-storage.service';

import { BaseLayoutComponent } from './components/layout/base-layout.component';
import { BasicModalComponent } from './components/basic-modal/basic-modal.component';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        RouterModule
    ],
    exports: [
        BaseLayoutComponent,
        BasicModalComponent,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule
    ],
    declarations: [BaseLayoutComponent, BasicModalComponent],
    providers: [
        ContactService,
        LocalStorageService
    ],
})
export class SharedModule { }
