import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

import { Contato } from '../../models/Contato';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class ContactService {

  constructor(private localStore : LocalStorageService) {}

  getAll() : Observable<Contato[]> {
    return Observable.create(observer => {
      let contacts = this.localStore.getValue('contacts');
      if(contacts) {
        observer.next(contacts);
      } else {
        observer.next([]);
      }
      observer.complete();
    });
  }

  getById(id) : Observable<Contato> {
    return Observable.create(observer => {
      let contacts = this.localStore.getValue("contacts");
      if(!contacts) {
        observer.next(false);
        observer.complete();
      } else {
        let found = contacts.find((item) => item.id == id);
        let contato = new Contato(found);
        observer.next(contato);
        observer.complete();
      }
    });
  }

  updateById(id, contato : Contato) : Observable<boolean> {
    return Observable.create(observer => {
      let contacts = this.localStore.getValue("contacts");
      if(!contacts) {
        contacts = [];
      }

      let contactIndex = contacts.findIndex((contact) => contact.id == id);

      if(contactIndex < 0) {
        contacts.push(contato);
      } else {
        contacts[contactIndex] = contato;
      }

      this.localStore.setValue('contacts', contacts);
      observer.next(true);
      observer.complete();
    });
  }

  removeById(id) : Observable<boolean> {
    return Observable.create(observer => {
      let contacts = this.localStore.getValue('contacts');
      let contactIndex = -1;

      if(contacts) {
        contactIndex = contacts.findIndex(item => item.id == id);
      }

      if(contactIndex >= 0) {
        contacts.splice(contactIndex, 1);
        this.localStore.setValue('contacts', contacts);
      }

      observer.next(true);
      observer.complete();
    });
  }

  add(contato : Contato) : Observable<boolean> {
    let random, exists;
    let contacts = this.localStore.getValue('contacts');
    if(!contacts) {
      contacts = [];
    }
    
    // Generate id
    do {
      random = Math.floor(Math.random()*1000);
      exists = false;
      contacts.forEach(item => {
        if(item.id == random){
          exists = true;
        }
      });
    } while (exists);
    contato.id = random;

    return Observable.create(observer => {
      contacts.push(contato);
      this.localStore.setValue('contacts', contacts);
      observer.next(true);
      observer.complete();
    });
  }

}
