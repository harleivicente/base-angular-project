import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

  constructor() { }

  /**
   * Obtains a value from the local storage.
   * Will return the parsed value.
   * 
   * @param key
   * @return mixed false | any
   *  
   */
  getValue(name) {
    let result;
    try {
      let valueRaw = localStorage.getItem(name);
      result = JSON.parse(valueRaw);
    } catch (error) {
      result = false;
    }
    return result;
  }

  /**
   * 
   * @param name 
   * @param value raw js value  
   */
  setValue(name, value) {
    localStorage.setItem(name, JSON.stringify(value));
  }

}
